-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Tempo de geração: 04-Jun-2020 às 06:39
-- Versão do servidor: 10.4.10-MariaDB
-- versão do PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `project_funag`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `endereco`
--

DROP TABLE IF EXISTS `endereco`;
CREATE TABLE IF NOT EXISTS `endereco` (
  `ID_Endereco` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Pessoa` int(11) NOT NULL,
  `Endereco` text COLLATE utf16_bin NOT NULL,
  `Cidade` text COLLATE utf16_bin NOT NULL,
  `UF` text COLLATE utf16_bin NOT NULL,
  `CEP` varchar(10) COLLATE utf16_bin NOT NULL,
  PRIMARY KEY (`ID_Endereco`),
  KEY `fk_foreign_key_name` (`ID_Pessoa`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

--
-- Extraindo dados da tabela `endereco`
--

INSERT INTO `endereco` (`ID_Endereco`, `ID_Pessoa`, `Endereco`, `Cidade`, `UF`, `CEP`) VALUES
(14, 14, 'Terceira chacara acima do morro', 'Riborizal', 'PB', '23952-895'),
(15, 15, 'Chacaras nova moda', 'Cafezais do sul', 'SC', '62945-945'),
(16, 15, 'Setor de mansoes carissimas', 'Brasilia', 'DF', '17177-237'),
(17, 15, 'Condominio Nova capital Celestial apt 500', 'Xampanha', 'BA', '4626-129');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoa`
--

DROP TABLE IF EXISTS `pessoa`;
CREATE TABLE IF NOT EXISTS `pessoa` (
  `ID_Pessoa` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` text COLLATE utf16_bin NOT NULL,
  `Numero_Identidade` int(11) NOT NULL,
  `Senha` varchar(150) COLLATE utf16_bin NOT NULL,
  `Data_Aniversario` date NOT NULL,
  PRIMARY KEY (`ID_Pessoa`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

--
-- Extraindo dados da tabela `pessoa`
--

INSERT INTO `pessoa` (`ID_Pessoa`, `Nome`, `Numero_Identidade`, `Senha`, `Data_Aniversario`) VALUES
(14, 'Jonny da Silva', 12345, '202cb962ac59075b964b07152d234b70', '2000-02-25'),
(15, 'Matias da Silva', 456789, '202cb962ac59075b964b07152d234b70', '2001-02-14');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
