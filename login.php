<?php
require_once 'conexaodb.php'; // Chmando a página do banco

//Sessão
session_start();



//Botão form
if (isset($_POST['logar'])) {
	$erros = [];
	$numeroid = mysqli_escape_string($connect, $_POST["numeroid"]); //Função do mysql para filtragem dos dados digitados pelo user
	$senha = mysqli_escape_string($connect, $_POST['senha']); //Função do mysql para filtragem dos dados digitados pelo user

	if (empty($numeroid) or empty($senha)) { //Checando se os campos estão vazios
		echo '<div class="fixed-bottom"><div class="alert alert-danger" role="alert">
      Existem campos em branco!
    </div></div>';
	} else {
		$sql = "select Numero_Identidade from pessoa where Numero_Identidade = '$numeroid' "; //Consulta SQL
		$resultado = mysqli_query($connect, $sql); //Capturando o resultado
		if (mysqli_num_rows($resultado) > 0) { //Vendo se existe um resultado
			$senha = md5($senha);
			$sql = "select * from pessoa where Numero_Identidade = '$numeroid' and senha = '$senha' ";
			$resultado = mysqli_query($connect, $sql);

			if (mysqli_num_rows($resultado) > 0) { //Vendo se existe um resultado
				$dados = mysqli_fetch_array($resultado);
				'<div class="fixed-bottom"><div class="alert alert-success" role="alert">
              Logado com sucesso!
            </div></div>';
				$_SESSION['logado'] = true;
				$_SESSION['id_usuario'] = $dados['Nome'];
				header('Location: index.php');
			} else {
				echo '<div class="fixed-bottom"><div class="alert alert-danger" role="alert">
      Usuário ou senha errados.
    </div></div>';
			}
		} else {
			echo '<div class="fixed-bottom"><div class="alert alert-danger" role="alert">
      Usuário ou senha errados.
    </div></div>';
		} {
		}
	}
}

if (isset($_POST['cadastrar'])) {
	header('Location: cadastro.php');
}
?>



<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>FUNAG</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link href="./css/signin.css" rel="stylesheet">
</head>

<body class="text-center" style="background-color:#E6E6E6;">
	<form class="form-signin" method="post">
		<img class="mb-4" src="img\logo.png" alt="" width="72" height="72">
		<h1 class="h3 mb-3 font-weight-normal">Área de login</h1>
		<label for="inputEmail" class="sr-only">Numero da Identidade</label>
		<input type="number" name="numeroid" class="form-control" placeholder="Identidade">
		<label for="inputPassword" class="sr-only">Senha</label>
		<input type="password" name="senha" class="form-control" placeholder="*******">

		<button class="btn btn-lg btn-primary btn-block" type="submit" name="logar">Logar</button>
		<button class="btn btn-outline-primary btn-block" name="cadastrar">Criar conta</button>
	</form>
</body>

<div class="fixed-bottom" style="margin-bottom:8% ;"><p style="text-align: center;">Feito por: João Victor Correia de Oliveira</p> </div>
</html>