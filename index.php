<?php

require_once 'conexaodb.php'; // Chmando a página do banco

//Sessão
session_start();

if (isset($_SESSION['id_usuario'])) {
	$id = $_SESSION['id_usuario'];
}


if (isset($_POST['sair'])) {
	header('Refresh:0;');
	session_unset();
}

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>FUNAG</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>

<body style="background-color:#E6E6E6;">
	<nav class="navbar navbar-expand-lg  bg-light" style="background-color: #e3f2fd;">
		<a class="navbar-brand" href=".\index.php">
			<img src="img\logo.png" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
			FUNAG
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a href=".\cadastro.php" name="relatorio" class="nav-link">Cadastro de Pessoas</a>

				</li>

				<li class="nav-item">
					<a class="nav-link" href=".\cadastroend.php">Cadastro de Endereços</a>
				</li>

				<li class="nav-item">
					<a href=".\generatepdf.php" name="relatorio" class="nav-link">Gerar relatório</a>
				</li>


			</ul>
			<p style="margin-right: 3%;margin-top:8px; color:#3E4095;">
				<?php
				if (isset($id)  && isset($_SESSION['id_usuario'])) {
					echo 'Bem-vindo, ' . $id;
				}
				?>
			</p>
			<form action="" method="post">
				<?php
				if (isset($id)  && isset($_SESSION['id_usuario'])) {

					echo '<button class="btn btn-outline-primary my-2 my-sm-0" type="submit" name="sair" >Sair</button>';
				} else {
					echo  '<a  class="btn btn-outline-primary my-2 my-sm-0" href=".\login.php">Login</a>';
				}
				?>

			</form>

		</div>
	</nav>




	<footer>
		<div class="fixed-bottom" style="margin-bottom:8% ;">
			<p style="text-align: center;">Feito por: João Victor Correia de Oliveira</p>
		</div>
		<script src=".\js\jquery.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	</footer>

</body>

</html>