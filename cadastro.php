<?php 
require_once 'conexaodb.php'; // Chmando a página do banco

//Sessão
session_start();

if (isset($_SESSION['id_usuario'])) {
	$id = $_SESSION['id_usuario'];
}


if (isset($_POST['sair'])) {
	header('Refresh:0;');
	session_unset();
}




//Botão form
if(isset($_POST['cadastrar'])){
    $mensagens = [];
    $nome = mysqli_escape_string($connect, $_POST["nome"]);//Função do mysql para filtragem dos dados digitados pelo user
    $numeroid = mysqli_escape_string($connect, $_POST["numeroid"]);
    $senha = md5(mysqli_escape_string($connect, $_POST['senha']));
    $rawdate = htmlentities($_POST['niver']);
    $niver = date('Y-m-d', strtotime($rawdate));
    $endereco1 = (mysqli_escape_string($connect, $_POST['endereco1']));
    $cidade1 = (mysqli_escape_string($connect, $_POST['cidade1']));
    $uf1 = (mysqli_escape_string($connect, $_POST['uf1']));
    $cep1 = (mysqli_escape_string($connect, $_POST['cep1']));
    $endereco2 = (mysqli_escape_string($connect, $_POST['endereco2']));
    $cidade2 = (mysqli_escape_string($connect, $_POST['cidade2']));
    $uf2 = (mysqli_escape_string($connect, $_POST['uf2']));
    $cep2 = (mysqli_escape_string($connect, $_POST['cep2']));

    if(empty($nome) or empty($senha)or empty($numeroid) or empty($endereco1) or empty($cidade1) or empty($uf1) or empty($cep1) ){ //Checando se os campos estão vazios
      echo '<div class="fixed-bottom"><div class="alert alert-danger" role="alert">
      Existem campos em branco!
    </div></div>';
    }
    else{
      if(isset($_POST['checkbox'])){
        $sql = "INSERT INTO `pessoa` (`ID_Pessoa`, `Nome`,`Numero_Identidade` , `Senha`, `Data_Aniversario`) VALUES
        (NULL, '".$nome."','".$numeroid."', '".$senha."','".$niver."');"; //Consulta SQL

        if ($connect->query($sql) === TRUE) {
          $id_pessoa = mysqli_insert_id($connect);
          if($endereco2 != "" ){
           $sql = "INSERT INTO `endereco` (`ID_Endereco`, `ID_Pessoa`, `Endereco`, `Cidade`, `UF`, `CEP`) VALUES
            (NULL,'".$id_pessoa."' , '".$endereco1."', '".$cidade1."', '".$uf1."', '".$cep1."'),
            (NULL, '".$id_pessoa."', '".$endereco2."', '".$cidade2."', '".$uf2."', '".$cep2."');";

             if ($connect->query($sql) === TRUE) {
              echo '<div class="fixed-bottom"><div class="alert alert-success" role="alert">
              Usuário criado com sucesso!
            </div></div>';
             }
             else{
              echo '<div class="fixed-bottom"><div class="alert alert-danger" role="alert">
              Erro ao criar usuário!
            </div></div>';

             }
          }else{
            $sql = "INSERT INTO `endereco` (`ID_Endereco`, `ID_Pessoa`, `Endereco`, `Cidade`, `UF`, `CEP`) VALUES
              (NULL,'".$id_pessoa."' , '".$endereco1."', '".$cidade1."', '".$uf1."', '".$cep1."');";
            if ($connect->query($sql) === TRUE) {
              echo '<div class="fixed-bottom"><div class="alert alert-success" role="alert">
              Usuário criado com sucesso!
            </div></div>';
            }
            else{
              echo '<div class="fixed-bottom"><div class="alert alert-danger" role="alert">
              Erro ao criar usuário!
            </div></div>';

            }
          }

      } else {
        echo "<div class='alert alert-danger' role='alert'>
            'Error:' . $sql . '<br>' . $connect->error;
            </div>";
      }
      }
        else{
          echo '<div class="fixed-bottom"><div class="alert alert-danger" role="alert">
              Você deve concordar com os temos para criar o usuário.
            </div></div>';

        }
        
    }
    $connect->close();
}
?>


<html>
     <head>
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>FUNAG</title>
          <!-- bootstrap -->
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
     </head>
     <body style="background-color:#E6E6E6;">
     <nav class="navbar navbar-expand-lg  bg-light" style="background-color: #e3f2fd;">
     <a class="navbar-brand" href=".\index.php">
    <img src="img\logo.png" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
    FUNAG
  </a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
			<li class="nav-item">
					<a href=".\cadastro.php" name="relatorio" class="nav-link" >Cadastro de Pessoas</a>
				
				</li>

				<li class="nav-item">
					<a class="nav-link" href=".\cadastroend.php">Cadastro de Endereços</a>
				</li>

				<li class="nav-item">
				<a href=".\generatepdf.php" name="relatorio" class="nav-link" >Gerar relatório</a>
				</li>

                    
			</ul>
			<p style="margin-right: 3%;margin-top:8px; color:#3E4095;">
				<?php
				if (isset($id)  && isset($_SESSION['id_usuario'])) {
					echo 'Bem-vindo, ' . $id;
				}
				?>
			</p>
			<form action="" method="post">
				<?php
				if (isset($id)  && isset($_SESSION['id_usuario'])) {

					echo '<button class="btn btn-outline-primary my-2 my-sm-0" type="submit" name="sair" >Sair</button>';
				} else {
					echo  '<a  class="btn btn-outline-primary my-2 my-sm-0" href=".\login.php">Login</a>';
				}
				?>

			</form>

		</div>
	</nav>
          <div style="width: 50%; height: 50%; margin-left: 25%; margin-top: 5%;">
          <form  method="post">
               <div class="form-row">
                 <div class="form-group col-md-6">
                   <label for="inputEmail4">Nome completo</label>
                   <input type="text" class="form-control" name="nome" aria-required="true">
                 </div>
                 <div class="form-group col-md-6">
                   <label for="inputPassword4">Senha</label>
                   <input type="password" class="form-control" name="senha" aria-required="true">
                 </div>
               </div>
              
               <div class="form-row">
                 <div class="form-group col-md-6">
                 <label for="inputAddress">Número Identidade</label>
                  <input type="number" class="form-control" name="numeroid" placeholder="99999" style="width: 110%;" aria-required="true">
     
                 </div>
                 <div class="form-group col-md-6">
                   <label for="inputPassword4" style="margin-left: 10%;">Data de nascimento</label>
                   <input type="date" style="width: 85%; margin-left:10%" class="form-control" name="niver" aria-required="true">
                 </div>
               </div>
               <div class="form-group">
              <label for="inputAddress">Endereço</label>
              <input type="text" class="form-control" name="endereco1" placeholder="2ª Avenida Bloco 1250 CASA 03">
            </div>

               <div>
                   
                       <div class="form-row">
                         <div class="form-group col-md-6">
                           <label for="inputCity">Cidade</label>
                           <input type="text" class="form-control" name="cidade1" aria-required="true">
                         </div>
                         <div class="form-group col-md-4">
                           <label for="inputState">UF</label>
                           <select name="uf1" class="form-control" aria-required="true">
                             <option selected>-</option>
                             <option value="AC">AC</option>
                         <option value="AL">AL</option>
                         <option value="AM">AM</option>
                         <option value="AP">AP</option>
                         <option value="BA">BA</option>
                         <option value="CE">CE</option>
                         <option value="DF">DF</option>
                         <option value="ES">ES</option>
                         <option value="GO">GO</option>
                         <option value="MA">MA</option>
                         <option value="MG">MG</option>
                         <option value="MS">MS</option>
                         <option value="MT">MT</option>
                         <option value="PA">PA</option>
                         <option value="PB">PB</option>
                         <option value="PE">PE</option>
                         <option value="PI">PI</option>
                         <option value="PR">PR</option>
                         <option value="RJ">RJ</option>
                         <option value="RN">RN</option>
                         <option value="RO">RO</option>
                         <option value="RR">RR</option>
                         <option value="RS">RS</option>
                         <option value="SC">SC</option>
                         <option value="SE">SE</option>
                         <option value="SP">SP</option>
                         <option value="TO">TO</option>        
                           </select>
                         </div>
                         <div class="form-group col-md-2">
                           <label for="inputZip">CEP</label>
                           <input type="text" style="width: 110%;" class="form-control" name="cep1" aria-required="true" onkeypress="mascara(this, '#####-###')" maxlength="9">
                         </div>

                        
               </div>   
               
               <div class="form-group">
                    <label for="inputAddress">Endereço 2 (Não opcional)</label>
                    <input type="text" class="form-control" name="endereco2" placeholder="2ª Avenida Bloco 1250 CASA 03">
               </div>
               <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="inputCity">Cidade</label>
                      <input type="text" class="form-control" name="cidade2">
                    </div>
                    <div class="form-group col-md-4">
                      <label for="inputState">UF</label>
                      <select name="uf2" class="form-control" aria-required="true" >
                         <option selected>-</option>
                         <option value="AC">AC</option>
                         <option value="AL">AL</option>
                         <option value="AM">AM</option>
                         <option value="AP">AP</option>
                         <option value="BA">BA</option>
                         <option value="CE">CE</option>
                         <option value="DF">DF</option>
                         <option value="ES">ES</option>
                         <option value="GO">GO</option>
                         <option value="MA">MA</option>
                         <option value="MG">MG</option>
                         <option value="MS">MS</option>
                         <option value="MT">MT</option>
                         <option value="PA">PA</option>
                         <option value="PB">PB</option>
                         <option value="PE">PE</option>
                         <option value="PI">PI</option>
                         <option value="PR">PR</option>
                         <option value="RJ">RJ</option>
                         <option value="RN">RN</option>
                         <option value="RO">RO</option>
                         <option value="RR">RR</option>
                         <option value="RS">RS</option>
                         <option value="SC">SC</option>
                         <option value="SE">SE</option>
                         <option value="SP">SP</option>
                         <option value="TO">TO</option>
                   
                       </select>
                    </div>
                    <div class="form-group col-md-2">
                      <label for="inputZip">CEP</label>
                      <input type="text" class="form-control" name="cep2" onkeypress="mascara(this, '#####-###')" maxlength="9" style="width: 110%;">
                    </div>
               </div>  
          
               <div class="form-group">
                 <div class="form-check">
                   <input class="form-check-input" type="checkbox" name="checkbox">
                   <label class="form-check-label" for="gridCheck">
                     Eu concordo com os termos
                   </label>
                 </div>
               </div>
               <button type="submit" class="btn btn-primary" name="cadastrar">Cadastrar</button>
             </form>
          </div>
      
          <footer >        
          <div class="fixed-bottom" style="margin-bottom:8% ;"><p style="text-align: center;">Feito por: João Victor Correia de Oliveira</p> </div>
                    <script src="./js/jquery.js"></script>
                    <script src="./js/scripts.js"></script>
               <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
          </footer>
     </body>
</html>